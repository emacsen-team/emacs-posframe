emacs-posframe (1.4.4-1) unstable; urgency=medium

  * New upstream version 1.4.4
  * Set upstream metadata fields: Bug-Submit.
  * Update Standards-Version to 4.7.0; no change needed
  * Update d/gbp.conf to match current practice
  * Add myself to Uploaders
  * Add Upstream-Contact in d/copyright

 -- Xiyue Deng <manphiz@gmail.com>  Mon, 25 Nov 2024 22:54:37 -0800

emacs-posframe (1.4.2-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:51:17 +0900

emacs-posframe (1.4.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * source only upload to enable migration (Closes: #1060413)

 -- Paul Gevers <elbrus@debian.org>  Wed, 10 Jan 2024 21:54:50 +0100

emacs-posframe (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2.
  * Update copyright years.
  * Drop boilerplate copyright paragraph.
  * Update section from lisp to editors.
  * Add upstream metadata.
  * Bump Standards-Version. No changes required.

 -- Raúl Benencia <rul@debian.org>  Fri, 08 Dec 2023 08:05:57 -0800

emacs-posframe (1.1.7-3) unstable; urgency=medium

  * fix d/watch
  * source only upload

 -- Thomas Koch <thomas@koch.ro>  Thu, 14 Apr 2022 20:58:42 +0300

emacs-posframe (1.1.7-2) unstable; urgency=medium

  * Correct Copyright holder to FSF.

 -- Thomas Koch <thomas@koch.ro>  Tue, 05 Apr 2022 21:37:50 +0300

emacs-posframe (1.1.7-1) unstable; urgency=medium

  * Initial release. Closes: #1007158.

 -- Thomas Koch <thomas@koch.ro>  Fri, 11 Mar 2022 19:28:07 +0200
